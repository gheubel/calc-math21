import random
import numpy as np

def provide_values():
    print("What are the dimensions of your augmented matrix?")
    print("Row count (n): ",end="")
    n = int(input())
    print("Column count (k): ",end="")
    k = int(input())

    print("Input every integer in the augmented coefficient matrix, separated by spaces: ",end="")
    matrix_nums = input().split(" ")

    if len(matrix_nums) != n*k:
        print(f"The dimensions of your matrix do not properly much the number of entries you provided. Please provide {n*k} entries for a {n} x {k} size matrix.")
        return provide_values()

    return (n, k, matrix_nums)

def gaussian_elim(aug, n, k):
    r = {i:aug[i-1] for i in range(1,len(aug)+1)}
    piv = [(i,i) for i in range(n)]

    for i in range(n):
        if (aug[i][0] != 1):
            aug[i] /= aug[i][0]
            if i != 0: 
                aug[i] -= aug[0]
            if i != 1: aug[i] -= aug[1]

        print(f"{aug}\n")

    # steps:
    # all the pivots need to be 1, so we first check if 0,0 is 1 and if it isnt we divide the row by the value at 0,0
    # do this for all rows for convenience
    # then ajfbaeljhgb

    return aug

n, k, matrix_nums = provide_values()
matrix_nums = [int(x) for x in matrix_nums]
aug_matrix = np.array(np.array_split(matrix_nums, n), dtype=float)

print(f"Prior augmented matrix:\n{aug_matrix}\n")
print(f"n: {n}, k: {k}\n")
gaussian_elim(aug_matrix, n, k)